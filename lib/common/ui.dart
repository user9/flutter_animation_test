import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Ui {
  static Widget scaffold({
    @required Widget body,
    Widget bar,
    EdgeInsetsGeometry insets,
    Color bodyColor = Colors.white,
  }) {
    if (Platform.isAndroid) {
      return Scaffold(
        appBar: bar,
        body: Container(
          color: bodyColor == null ? Colors.white : bodyColor,
          child: body,
          padding: insets,
        ),
      );
    }
    if (Platform.isIOS) {
      return CupertinoPageScaffold(
        navigationBar: bar,
        backgroundColor: Colors.white,
        child: Container(
          color: Colors.white,
          child: body,
          padding: insets,
        ),
      );
    }

    return null;
  }

  static Widget flatButton(
    String title,
    VoidCallback onPressed, {
    double fontSize,
    TextStyle style,
  }) {
    Widget textChild = Text(
      title,
      style: style == null ? TextStyle(fontSize: fontSize ?? 15) : style,
    );
    if (Platform.isAndroid) {
      return FlatButton(onPressed: onPressed, child: textChild);
    }
    if (Platform.isIOS) {
      return CupertinoButton(child: textChild, onPressed: onPressed);
    }
    return null;
  }
}

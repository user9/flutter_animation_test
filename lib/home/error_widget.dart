import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ftest/home/home_bloc.dart';

class BottomErrorWidget extends StatefulWidget {
  final int animationDuration;
  final double height;
  final GestureTapCallback onTapClose;
  final HomeBloc bloc;

  const BottomErrorWidget({
    Key key,
    @required this.animationDuration,
    @required this.onTapClose,
    @required this.bloc,
    @required this.height,
  }) : super(key: key);

  @override
  _BottomErrorWidgetState createState() => _BottomErrorWidgetState();
}

class _BottomErrorWidgetState extends State<BottomErrorWidget> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _offsetAnimation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: Duration(milliseconds: widget.animationDuration),
      vsync: this,
    );
    _offsetAnimation = Tween<Offset>(
      end: Offset.zero,
      begin: Offset(0.0, 1.0),
    ).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: widget.bloc.bottomWidgetVisible,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }
          if (snapshot.data) {
            _controller.forward();
          } else {
            _controller.reverse();
          }

          return SlideTransition(
            position: _offsetAnimation,
            child: _buildBody(),
          );
        });
  }

  Widget _buildBody() {
    const radius = 20.0;
    const insets = 20.0;

    return GestureDetector(
      onTap: () {},
      child: Container(
        height: widget.height,
        padding: const EdgeInsets.symmetric(horizontal: insets),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.only(
              topLeft: const Radius.circular(radius),
              topRight: const Radius.circular(radius),
            )),
        child: Material(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: insets),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    GestureDetector(
                      child: const Icon(Icons.close),
                      onTap: widget.onTapClose,
                    ),
                  ],
                ),
              ),
              const Text(
                'Somethig went wrong',
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              const SizedBox(height: insets),
              const Text(
                "We couldn't process your request. Please try again.",
                style: const TextStyle(fontSize: 18),
              ),
              Expanded(child: Container()),
              Row(
                children: <Widget>[
                  Expanded(
                    child: CupertinoButton(
                      color: Colors.yellow.shade700,
                      child: const Text(
                        'Try again',
                        style: const TextStyle(color: Colors.black),
                      ),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}

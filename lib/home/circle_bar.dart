import 'dart:math';

import 'package:flutter/material.dart';

class CircularProgress extends StatelessWidget {
  final double value;
  CircularProgress({this.value});

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      foregroundPainter: CircularBar(offset: Offset(20, 20), endAngle: (pi * 2 * value), radius: 30),
    );
  }
}

class CircularBar extends CustomPainter {
  var offset = Offset(0, 0);
  var radius = 30.0;
  var endAngle = (pi * 2 * 0.5);

  CircularBar({this.offset, this.radius, this.endAngle});
  @override
  void paint(Canvas canvas, Size size) {
    var p = Paint()
      ..color = Colors.white
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawArc(Rect.fromCircle(center: offset, radius: radius), -pi / 2, endAngle, false, p);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

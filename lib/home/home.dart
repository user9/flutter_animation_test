import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:ftest/home/circle_bar.dart';
import 'package:ftest/home/home_bloc.dart';

import '../common/ui.dart';
import 'error_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  final _bloc = HomeBloc();
  final _errorColor = Color.fromRGBO(40, 43, 59, 1);
  final _mainAnimation = 300;
  final _barAnimation = 200;
  final _barFillAnimation = 600;
  final _bottomWidgetAnimation = 420;
  final _errorTextAnimation = 420;
  final _bottomWidgetHeight = 300.0;

  AnimationController _circleAnimationController;
  Animation _circleAnimation;
  bool _visible = true;
  bool _iconVisible = false;
  bool _bottomWidgetVisible = false;
  bool _isErrorShowed = false;

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
    _circleAnimationController.dispose();
  }

  @override
  void initState() {
    super.initState();

    _circleAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: _barFillAnimation),
    );

    _circleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(_circleAnimationController)
      ..addStatusListener((AnimationStatus status) {
        switch (status) {
          case AnimationStatus.forward:
            break;
          case AnimationStatus.completed:
            showErrorIcon();
            break;
          case AnimationStatus.reverse:
            break;
          case AnimationStatus.dismissed:
            break;
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: showErrorScreen,
      child: GestureDetector(
        onTap: _isErrorShowed ? hideError : null,
        child: Container(
          color: _errorColor,
          child: Stack(
            children: <Widget>[
              Positioned.fill(
                child: AnimatedOpacity(
                  opacity: _visible ? 1.0 : 0.0,
                  duration: Duration(milliseconds: _mainAnimation),
                  child: Ui.scaffold(
                    body: _buildBody(),
                  ),
                ),
              ),
              _buildErrorIcon(),
              _buildCancel(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(bottom: 20),
            color: Colors.grey.shade200,
            child: Column(
              children: <Widget>[
                _buildHeader(),
                _buildMain(),
              ],
            ),
          ),
          _buildFooter(),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    const double imgScale = 7.0;

    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8, top: 60),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image.asset(
            'assets/img/alfa.png',
            scale: imgScale,
          ),
          Image.asset(
            'assets/img/mc_sc.png',
            scale: imgScale,
          ),
        ],
      ),
    );
  }

  Widget _buildMain() {
    final labelStyle = TextStyle(color: Colors.grey.shade700, height: 2, fontWeight: FontWeight.w500);
    const valueStyle = const TextStyle(height: 2, fontWeight: FontWeight.w500);

    return Padding(
      padding: const EdgeInsets.only(left: 16, top: 40),
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Торговая точка', style: labelStyle),
              Text('Сумма', style: labelStyle),
              Text('Номер карты', style: labelStyle),
              Text('Дата', style: labelStyle),
            ],
          ),
          const SizedBox(width: 40),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Text('YANDEX.DIRECT', style: valueStyle),
              const Text('1 180 р', style: valueStyle),
              const Text('..5856', style: valueStyle),
              const Text('27 ноября 2018', style: valueStyle),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildFooter() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text(
                'Введите пароль и подтвердите оплату',
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Container(
            width: 200,
            child: Material(
              color: Colors.white,
              child: const TextField(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Material(
              color: Colors.white,
              child: InkWell(
                onTap: () {},
                child: DottedBorder(
                  color: Colors.grey,
                  child: const Text('Получить пароль повторно'),
                  padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCancel() {
    return StreamBuilder<bool>(
        stream: _bloc.showErrorCircleStream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return Container();
          }
          return Positioned(
            bottom: 20,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Ui.flatButton(
                  'Отменить оплату',
                  () {},
                  style: const TextStyle(
                    color: Colors.grey,
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildErrorIcon() {
    return StreamBuilder<bool>(
        stream: _bloc.showErrorCircleStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData || !snapshot.data) {
            return Container();
          }
          return Stack(
            children: <Widget>[
              Center(
                child: AnimatedContainer(
                  width: 120,
                  height: 120,
                  duration: Duration(milliseconds: _bottomWidgetAnimation),
                  margin: _bottomWidgetVisible ? EdgeInsets.only(bottom: _bottomWidgetHeight) : EdgeInsets.only(bottom: 0),
                  child: _buildCircularAnimation(),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: BottomErrorWidget(
                  height: _bottomWidgetHeight,
                  bloc: _bloc,
                  animationDuration: _bottomWidgetAnimation,
                  onTapClose: hideError,
                ),
              ),
            ],
          );
        });
  }

  Widget _buildCircularAnimation() {
    return Stack(
      children: <Widget>[
        AnimatedBuilder(
            animation: _circleAnimationController,
            builder: (_, child) {
              return Transform.scale(
                scale: (_circleAnimationController.value * 0.2) + 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 60,
                      height: 60,
                      padding: const EdgeInsets.all(10),
                      child: Stack(
                        children: <Widget>[
                          CircularProgress(value: _circleAnimationController.value),
                          AnimatedOpacity(
                            opacity: _iconVisible ? 1.0 : 0.0,
                            duration: Duration(milliseconds: _barAnimation),
                            child: Center(
                              child: Material(
                                color: _errorColor,
                                child: IconButton(
                                  icon: const Icon(
                                    Icons.close,
                                    color: Colors.white,
                                    size: 25,
                                  ),
                                  onPressed: () => hideError(),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _errorColor,
                      ),
                    ),
                  ],
                ),
              );
            }),
        Positioned(
          bottom: 10,
          left: 0,
          right: 0,
          child: AnimatedOpacity(
            opacity: _iconVisible ? 1.0 : 0.0,
            duration: Duration(milliseconds: _errorTextAnimation),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Material(
                  color: _errorColor,
                  child: const Text(
                    'Error',
                    style: const TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void hideError() {
    setState(() {
      _bottomWidgetVisible = false;
    });
    _bloc.bottomWidgetVisible.add(false);
    Future.delayed(Duration(milliseconds: _bottomWidgetAnimation), () {
      setState(() {
        _visible = true;
        _iconVisible = false;
      });
      _bloc.showErrorIconStream.add(false);
      _bloc.showErrorCircleStream.add(false);

      Future.delayed(Duration(milliseconds: _mainAnimation), () {
        setState(() {
          _isErrorShowed = false;
          _circleAnimationController.reverse();
        });
      });
    });
  }

  void showErrorScreen() {
    setState(() {
      _visible = false;
    });
    Future.delayed(Duration(milliseconds: _mainAnimation), () {
      _bloc.showErrorCircleStream.add(true);
      _circleAnimationController.forward();
    });
  }

  void showErrorIcon() {
    setState(() {
      _iconVisible = true;
    });
    Future.delayed(Duration(milliseconds: _barAnimation * 2), () {
      setState(() {
        _bottomWidgetVisible = true;
      });
      _bloc.bottomWidgetVisible.add(true);
    });
    Future.delayed(Duration(milliseconds: _bottomWidgetAnimation), () {
      setState(() {
        _isErrorShowed = true;
      });
    });
  }
}

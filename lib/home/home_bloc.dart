import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final showErrorCircleStream = BehaviorSubject<bool>();
  final showErrorIconStream = BehaviorSubject<bool>();
  final bottomWidgetVisible = BehaviorSubject<bool>();

  void dispose() {
    showErrorCircleStream.close();
    showErrorIconStream.close();
    bottomWidgetVisible.close();
  }
}

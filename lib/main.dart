import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ftest/home/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget home = HomeScreen();

    if (Platform.isIOS) {
      return CupertinoApp(
        home: home,
        theme: CupertinoThemeData(
          scaffoldBackgroundColor: CupertinoColors.white,
        ),
      );
    }

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: home,
    );
  }
}
